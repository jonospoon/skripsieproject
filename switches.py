import os
import webapp2
import jinja2
import logging
from google.appengine.ext import ndb
import MonitorVariables as mon

JINJA_ENVIRONMENT = jinja2.Environment(loader=jinja2.FileSystemLoader(os.path.dirname(__file__)), 
extensions=['jinja2.ext.autoescape'], autoescape=True)

class createSwitch(webapp2.RequestHandler):
	def post(self):
		s_name_ = self.request.get("switch_name")
		i_NOTo_ = mon.checkbool(self.request.get("input_NOToutput"))
		dam_name_ = self.request.get("dam_name")
		arduino_pin_ =int (self.request.get("arduino_pin"))
		switch_check =mon.switchItem.query(ndb.AND(mon.switchItem.d_name==dam_name_,
				mon.switchItem.s_name==s_name_)).get()
		if(switch_check is None):
			pin_check =mon.switchItem.query(ndb.AND(mon.switchItem.d_name==dam_name_,
				mon.switchItem.arduino_pin== arduino_pin_)).get()
			if (pin_check is None):
				switchItem_ = mon.switchItem(d_name=dam_name_,s_name=s_name_,i_NOTo=i_NOTo_,arduino_pin = arduino_pin_)
				switchItem_.put()
				status_='Switch added'
			else: 
				status_='Pin already assigned'
		else:
			status_='Switch already exists'
		self.response.write(status_)

	def get(self):
		template = JINJA_ENVIRONMENT.get_template('templates/switchCreate.html')
		self.response.write(template.render())

class switchList(webapp2.RequestHandler):
	def get(self):
		logging.error('Contains nothing')
		template_values = {
			'switches' : ''
	   	}
	   	template = JINJA_ENVIRONMENT.get_template('switch.html')
		self.response.write(template.render(template_values))

	def post(self):
		dam_name_ = self.request.get("dam_name")
		switches_list = mon.switchItem.query(mon.switchItem.d_name==dam_name_).fetch()
		template_values = {
				'switches' : switches_list
		}
		template = JINJA_ENVIRONMENT.get_template('templates/switchList.html')
		self.response.write(template.render(template_values))

class switchChange(webapp2.RequestHandler):
	def post(self):
		"""A method to change the state of output switches from the website"""
		s_name_ = self.request.get("switch_name")
		dam_name_ = self.request.get("dam_name")
		switch_state_ = mon.checkbool(self.request.get("switch_state"))
		arduino_pin_ = str(self.request.get("arduino_pin"))
		#logging.info(switch_state_)
		switchChangeCheck_ = mon.switchChangeCheck.query(mon.switchChangeCheck.d_name==dam_name_).fetch()
		switch_ = mon.switchItem.query(ndb.AND(mon.switchItem.d_name==dam_name_,
				mon.switchItem.s_name==s_name_)).get()
		not_switch_state_ = not  (switch_state_)
		#logging.info( not_switch_state_)
		switch_.state =   not_switch_state_
		switch_.put()
		if switchChangeCheck_[0].change == False:
			switchChangeCheck_[0].change = True
			switchChangeCheck_[0].what_has_changed = (arduino_pin_)+","
			switchChangeCheck_[0].put()
		else:
			for i in switchChangeCheck_:
				x = i.what_has_changed.split(",")
				added = False
				for pins in x:
					if pins == (arduino_pin_):
						added = True
				if added == False:
					logging.info('Im new added')
					i.what_has_changed += (arduino_pin_)+","
					i.put()
				else: 
					logging.info('Im already added')
					i.what_has_changed = ''
					for pins in x:
						logging.info(pins)
						if pins != arduino_pin_:
							logging.info(pins)
							if pins != "":
								logging.info("I made it through")
								i.what_has_changed += pins+","
					if i.what_has_changed == "":
						i.what_has_changed = ""
						i.change = False
					i.put()


				
				
		switches_list = mon.switchItem.query(mon.switchItem.d_name==dam_name_).fetch()
		template_values = {
				'switches' : switches_list
		}
		template = JINJA_ENVIRONMENT.get_template('templates/switchList.html')
		self.response.write(template.render(template_values))
class switchDelete(webapp2.RequestHandler):
	def post(self):
		s_name_ = self.request.get("switch_name")
		dam_name_ = self.request.get("dam_name")
		switch_ = mon.switchItem.query(ndb.AND(mon.switchItem.d_name==dam_name_,
				mon.switchItem.s_name==s_name_)).fetch()
		logging.info(switch_)
		for switches in switch_:
			switches.key.delete()
		switches_list = mon.switchItem.query(mon.switchItem.d_name==dam_name_).fetch()
		template_values = {
				'switches' : switches_list
		}
		template = JINJA_ENVIRONMENT.get_template('templates/switchList.html')
		self.response.write(template.render(template_values))


