import os
import webapp2
import jinja2
import logging
from google.appengine.ext import ndb
import MonitorVariables as mon

JINJA_ENVIRONMENT = jinja2.Environment(loader=jinja2.FileSystemLoader(os.path.dirname(__file__)), 
extensions=['jinja2.ext.autoescape'], autoescape=True)


class measureList(webapp2.RequestHandler):
	def get(self):
		template = JINJA_ENVIRONMENT.get_template('measurement.html')
		self.response.write(template.render())
		
class deviceShow(webapp2.RequestHandler):
	def post(self):

		dam_name_ = self.request.get("dam_name")
		devices_list = mon.measurementItem.query(mon.measurementItem.d_name==dam_name_).fetch()
		template_values = {
				'devices' : devices_list
		}
		template = JINJA_ENVIRONMENT.get_template('templates/measureList.html')
		self.response.write(template.render(template_values))

class deviceDelete(webapp2.RequestHandler):
	def post(self):
		m_name_ = self.request.get("device_name")
		dam_name_ = self.request.get("dam_name")
		device_ = mon.measurementItem.query(ndb.AND(mon.measurementItem.d_name==dam_name_,
				mon.measurementItem.m_name==m_name_)).fetch()
		logging.info(device_)
		for devices in device_:
			devices.key.delete()
		devices_list = mon.measurementItem.query(mon.measurementItem.d_name==dam_name_).fetch()
		template_values = {
				'devices' : devices_list
		}
		template = JINJA_ENVIRONMENT.get_template('templates/measureList.html')
		self.response.write(template.render(template_values))

class deviceCreate(webapp2.RequestHandler):
	def post(self):
		item_name_ = self.request.get("device_name")
		dam_name_ = self.request.get("dam_name")
		arduino_pin_ = int( self.request.get("arduino_pin"))
		device_check =mon.measurementItem.query(ndb.AND(mon.measurementItem.d_name==dam_name_,
				mon.measurementItem.m_name==item_name_)).get()
		if(device_check is None):
			pin_check =mon.measurementItem.query(ndb.AND(mon.measurementItem.d_name==dam_name_,
				mon.measurementItem.arduino_pin== arduino_pin_)).get()
			if (pin_check is None):
				mon.measurementItem(d_name=dam_name_,m_name=item_name_,arduino_pin=arduino_pin_).put()
				status_='Device added'
			else:
				status_='Pin already assigned'
		else:
			status_='Device already exists'
		self.response.write(status_)

	def get(self):
		template = JINJA_ENVIRONMENT.get_template('templates/measureCreate.html')
		self.response.write(template.render())
