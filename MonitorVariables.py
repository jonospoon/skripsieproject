"""MonitorVariables"""
import os
import webapp2
import jinja2
import logging
from google.appengine.ext import ndb
from google.appengine.api import users
from google.appengine.ext.ndb import polymodel
import dams
import switches
import measurement

class inputSwitches(ndb.Model):
	"""Status of the switches"""
	switch1 = ndb.BooleanProperty(default = False)
	switch2 = ndb.BooleanProperty(default = False)
	switch3 = ndb.BooleanProperty(default = False)

class outputs(ndb.Model):
	Led1 = ndb.BooleanProperty(default = False)
	Led2 = ndb.BooleanProperty(default = False)
	Led3 = ndb.BooleanProperty(default = False)
	Relay1 = ndb.BooleanProperty(default = False)
	Relay2 = ndb.BooleanProperty(default = False)


class inputCurrents(ndb.Model):
	device1 = ndb.IntegerProperty()
	device2 = ndb.IntegerProperty()

class damItem(polymodel.PolyModel):
	d_name = ndb.StringProperty()
	date = ndb.DateTimeProperty(auto_now_add=True)

class switchItem(damItem):
	s_name 	= ndb.StringProperty()
	state 	= ndb.BooleanProperty(default = False)
	i_NOTo 	= ndb.BooleanProperty(default = False)
	arduino_pin = ndb.IntegerProperty()

class measurementlog(ndb.Model):
	array_values = ndb.FloatProperty()
	date_time = ndb.DateTimeProperty(auto_now_add=True)

class measurementItem(damItem):
	m_name = ndb.StringProperty()
	value = ndb.FloatProperty(default = 0)
	value_array = ndb.StructuredProperty(measurementlog,repeated=True)
	arduino_pin = ndb.IntegerProperty()

class location(damItem):
	location = ndb.StringProperty()

class switchChangeCheck(damItem):
	change = ndb.BooleanProperty(default = False)
	what_has_changed = ndb.StringProperty()


JINJA_ENVIRONMENT = jinja2.Environment(loader=jinja2.FileSystemLoader(os.path.dirname(__file__)), 
extensions=['jinja2.ext.autoescape'], autoescape=True)

class MainPage(webapp2.RequestHandler):
	def get(self):
		
		checker = checkexists()
		checker.check()
		switches = inputSwitches.query().fetch()
		template_values = {
			'switch':switches[0],
			'response':'hello'
	   	}
		template = JINJA_ENVIRONMENT.get_template('index.html')
		self.response.write(template.render(template_values))

class switchCheck(webapp2.RequestHandler):
	def get(self):
		
		switches = inputSwitches.query().fetch()
		template_values = {
			'switch' : switches[0]
	   	}
	   	template = JINJA_ENVIRONMENT.get_template('switch.html')
		self.response.write(template_values)

class switchChange(webapp2.RequestHandler):
	def get(self):
		self.request.get()

class checkexists(object):
	def check(self):
		switch_input = inputSwitches.query().get()
		output_block = outputs.query().get()
		input_current = inputCurrents.query().get()
		
		if (input_current is None):
			inputCurrents(device1 = 0, device2= 0).put()
			print("Im current")
		if (switch_input is None):
			inputSwitches(switch1=False,switch2=False,switch3=False).put()
			print("New switch")

class changeSwitch(webapp2.RequestHandler):
	def post(self):
		logging.info("starting")
		sw1_new_state = self.checkbool(self.request.get('sw1'))
		sw2_new_state = self.checkbool(self.request.get('sw2'))
		sw3_new_state = self.checkbool(self.request.get('sw3'))

		logging.info("sw3 %s",sw3_new_state)
		dbitem = inputSwitches.query().fetch()
		for x in dbitem:
			x.switch1=sw1_new_state
			x.switch2= sw2_new_state
			x.switch3=sw3_new_state
			x.put()

def checkbool(state):
		print state
		if (state == "true" or state == "True"):
			
			return True;
		elif(state == "false" or state =="False"):
			return False
		else:
			print 'true'
			return False

class checkWorks(webapp2.RequestHandler):
	def get(self):
		switch1 = switchItem(d_name='hart',location='False Bay',s_name='switch1', state=False,i_NOTo=False)
		switch2 = switchItem(d_name='hart',location='False Bay',s_name='switch2',state=False,i_NOTo=False)
		switch1.put()
		switch2.put()
		current1 = measurementItem(d_name= 'bes',location='False',m_name='in1')
		current1.put()
		for dams in damItem.query(damItem.class_ =='switchItem'):
			logging.info('dam name: %s\n name: %s\nstate: %s\ninput: %s'%(dams.d_name,dams.s_name, dams.state,dams.i_NOTo))
		c1 = damItem.query(damItem.class_ == 'measurementItem')
		for x in c1:
			logging.info(x.m_name)

app = webapp2.WSGIApplication([
    ('/', MainPage),
    ('/switchCheck', checkWorks),
    ('/changeSwitches', changeSwitch),
    ('/createDam', dams.createDam),
    ('/deleteDam', dams.deleteDam),
    ('/checkDam',dams.checkDam),
    ('/createSwitch',switches.createSwitch),
    ('/dams',dams.listDams),
    ('/switches',switches.switchList),
    ('/switchChange', switches.switchChange),
    ('/switchDelete',switches.switchDelete),
    ('/measuring', measurement.measureList),
    ('/deviceShow', measurement.deviceShow),
    ('/deviceDelete',measurement.deviceDelete),
    ('/deviceCreate', measurement.deviceCreate)

    
], debug=True)