"""API for MonitorDam app to access """

import endpoints
import os
import logging
logging.getLogger().setLevel(logging.DEBUG)
from protorpc import messages
from protorpc import message_types
from protorpc import remote
from google.appengine.ext import ndb
import MonitorVariables


class StatusOutput(messages.Message):
	"""Status type as output"""
	status = messages.StringField(1)

class switchStatus(messages.Message):
	"""Change the status of the switches"""
	switch1 = messages.BooleanField(1)
	switch2 = messages.BooleanField(2)
	switch3 = messages.BooleanField(3)

class dams(messages.Message):
	dam_name = messages.StringField(1)

class switches(messages.Message):
	dam_name  = messages.StringField(1)
	switch_name 	= messages.StringField(2)
	input_NOToutput 	= messages.BooleanField(3)
	arduino_pin = messages.IntegerField(4)

class measurementItem(messages.Message):
	dam_name  = messages.StringField(1)
	item_name 	= messages.StringField(2)
	arduino_pin = messages.IntegerField(3)

class item_general(messages.Message):
	dam_name  = messages.StringField(1)
	item_name 	= messages.StringField(2)

class switch_state(messages.Message):
	dam_name  = messages.StringField(1)
	switch_name 	= messages.StringField(2)
	State_on_Noff 	= messages.BooleanField(3)

class measurements(messages.Message):
	dam_name = messages.StringField(1)
	item_name = messages.StringField(2)
	value = messages.IntegerField(3)



DAM_RESOURCE = endpoints.ResourceContainer(dams)

SWITCH2_RESOURCE = endpoints.ResourceContainer(switches)

SWITCH_RESOURCE = endpoints.ResourceContainer(switchStatus)

SWITCH_CHANGE_RESOURCE = endpoints.ResourceContainer(switch_state)

ITEM_RESOURCE = endpoints.ResourceContainer(item_general)

MEASUREMENT_RESOURCE = endpoints.ResourceContainer(measurements)

ITEM_CREATE_RESOURCE = endpoints.ResourceContainer(measurementItem)

PING_RESOURCE = endpoints.ResourceContainer(message_types.VoidMessage,)


@endpoints.api(name='monitorDamAPI', version='v2')
class monitorDamAPI(remote.Service):
	"""MonitorDam API to interface Android app with App Engine Datastore"""

	@endpoints.method(DAM_RESOURCE, StatusOutput,
		path="checkForChange",http_method="POST",
		name="checkForChange")
	def checkForChange(self, request):
		"""Checks whether there has been any chnage on the output switches since the last check"""
		dam_name_ = request.dam_name
		switch_change_ = MonitorVariables.switchChangeCheck.query(MonitorVariables.switchChangeCheck.d_name==dam_name_).get()
		if switch_change_.change == True:
			status_=switch_change_.what_has_changed
			switch_change_.what_has_changed=None
			switch_change_.change=False
			switch_change_.put()
		else:
			status_= "None"
		return StatusOutput(status=status_)

	
	@endpoints.method(PING_RESOURCE,StatusOutput,
		path="ping",http_method="GET",
		name="ping")
	def pinger(self,request):
		"""Pings the server"""
		return StatusOutput(status="ping")

	@endpoints.method(ITEM_RESOURCE, switch_state,
		path="switchStatus", http_method="POST",
		name="switchStatus")
	def change_switches(self,request):
		"""Check the status of an switch"""
		dam_name_ = request.dam_name
		switch_name_ = request.item_name
		dbitem = MonitorVariables.switchItem.query(ndb.AND(MonitorVariables.switchItem.d_name==dam_name_,
				MonitorVariables.switchItem.s_name==switch_name_)).fetch()
		state = dbitem[0].state
		return switch_state(dam_name= dam_name_, switch_name=switch_name_,State_on_Noff=state )

	@endpoints.method(DAM_RESOURCE,StatusOutput,
		path="damCreate", http_method="POST",
		name="damCreate")
	def damCreate(self,request):
		"""Create a new dam if it does not already exist"""
		damName_new = request.dam_name
		new_dam = MonitorVariables.damItem.query(MonitorVariables.damItem.d_name==damName_new).get()
		logging.info(new_dam)
		if (new_dam is None):
			MonitorVariables.damItem(d_name=damName_new).put()
			outStatus = "New dam entered into the system"
		else:
			outStatus = "Dam already exists"
		return StatusOutput(status=outStatus)

	@endpoints.method(PING_RESOURCE, StatusOutput,
		path="damList", http_method="POST",
		name="damList")
	def damList(self,request):
		"""List all the current dams"""
		dams_list = MonitorVariables.damItem.query().fetch()
		#logging.info(dams_list)
		dam_temp =''
		damsss = ''
		for dam in dams_list:
			if (dam_temp != dam.d_name):
				dam_temp = dam.d_name
				damsss += (' Dam_name: %s |'% (dam.d_name))
				damsss += ''
		#logging.info(damsss)
		return StatusOutput(status= damsss)

	@endpoints.method(DAM_RESOURCE, StatusOutput,
		path="damDelete", http_method="POST",
		name="damDelete")
	def damDelete(self,request):
		"""Delete a dam specified by name"""
		dam_name_to_delete = request.dam_name
		dam_ = MonitorVariables.damItem.query(MonitorVariables.damItem.d_name == dam_name_to_delete).get()
		if (dam_ is None):
			status_ = 'Dam does not exist'
		else:
			dam_ = MonitorVariables.damItem.query(MonitorVariables.damItem.d_name == dam_name_to_delete).fetch()
			
			for dams in dam_:
				dams.key.delete()
			status_ = 'Dam deleted'
		return StatusOutput(status= status_)

	@endpoints.method(DAM_RESOURCE, StatusOutput,
		path="switchList", http_method="POST",
		name="switchList")
	def switchList(self,request):
		"""List all the current switches"""
		dam_wanted = request.dam_name
		dams_list = MonitorVariables.switchItem.query(MonitorVariables.switchItem.d_name==dam_wanted).fetch()
		#logging.info(dams_list)
		damsss = ''
		for dam in dams_list:
			damsss += (' Switch name:%s, Pin:%s, Input:%s, State:%s/'% (dam.s_name, dam.arduino_pin, dam.i_NOTo, dam.state))
			damsss += ''
		#logging.info(damsss)
		return StatusOutput(status= damsss)

	@endpoints.method(SWITCH2_RESOURCE, StatusOutput,
		path="switchCreate", http_method="POST",
		name="switchCreate")
	def switchCreate(self,request):
		"""Create a switch for the first time"""
		status_ = ''
		dam_name_ = request.dam_name
		s_name_ = request.switch_name
		i_NOTo_ = request.input_NOToutput
		arduino_pin_ = request.arduino_pin
		dam_check = MonitorVariables.damItem.query(MonitorVariables.damItem.d_name==dam_name_).get()
		if (dam_check is None):
			status_ = 'Dam does not exist'
		else:
			switch_check =MonitorVariables.switchItem.query(ndb.AND(MonitorVariables.switchItem.d_name==dam_name_,
				MonitorVariables.switchItem.s_name==s_name_)).get()
			if(switch_check is None):
				pin_check = MonitorVariables.switchItem.query(ndb.AND(MonitorVariables.switchItem.d_name==dam_name_,
				MonitorVariables.switchItem.arduino_pin== arduino_pin_)).get()
				if (pin_check is None):
						MonitorVariables.switchItem(d_name=dam_name_,s_name=s_name_,i_NOTo=i_NOTo_,arduino_pin=arduino_pin_).put()
						status_='switch added'
				else:
					status_="Pin already assigned"
			else:
				status_='Switch already exists'

		return StatusOutput(status=status_)
	@endpoints.method(SWITCH_CHANGE_RESOURCE, StatusOutput,
		path="switchChange", http_method="POST",
		name="switchChange")
	def switchChange(self, request):
		"""Change the status of a switch"""
		switch_name_ = request.switch_name
		dam_name_ = request.dam_name
		state_on = request.State_on_Noff
		switchCheck_ = MonitorVariables.switchItem.query(ndb.AND(MonitorVariables.switchItem.d_name==dam_name_,
				MonitorVariables.switchItem.s_name==switch_name_)).get()
		if (switchCheck_ is None):
			status_ = 'switch does not exist'
		else:
			switchItem_ = MonitorVariables.switchItem.query(ndb.AND(MonitorVariables.switchItem.d_name==dam_name_,
				MonitorVariables.switchItem.s_name==switch_name_)).fetch()
			for i in switchItem_:
				i.state = state_on
				i.put()
				status_ = ('switch change to %s' %(state_on))
		return StatusOutput(status = status_)

	@endpoints.method(ITEM_RESOURCE, StatusOutput,
		path="switchDelete", http_method="POST",
		name="switchDelete")
	def switchDelete(self,request):
		"""Delete a specific switch at a dam"""
		switch_name_ = request.item_name
		dam_name_ = request.dam_name
		switchCheck_ = MonitorVariables.switchItem.query(ndb.AND(MonitorVariables.switchItem.d_name==dam_name_,
				MonitorVariables.switchItem.s_name==switch_name_)).get()
		if switchCheck_ is None:
			status_ = 'Switch does not exist'
		else:
			switchItem_ = MonitorVariables.switchItem.query(ndb.AND(MonitorVariables.switchItem.d_name==dam_name_,
				MonitorVariables.switchItem.s_name==switch_name_)).fetch()
			for i in switchItem_:
				i.key.delete()
			status_ = 'Switch deleted'
		return StatusOutput(status = status_)


	@endpoints.method(ITEM_CREATE_RESOURCE, StatusOutput,
		path="measureCreate", http_method="POST",
		name="measureCreate")
	def measureCreate(self,request):
		"""Create a new measurement Item"""
		item_name_ = request.item_name
		dam_name_ = request.dam_name
		arduino_pin_ = request.arduino_pin
		dam_check = MonitorVariables.damItem.query(MonitorVariables.damItem.d_name==dam_name_).get()
		if (dam_check is None):
			status_ = 'Dam does not exist'
		else:
			itemCheck_ = MonitorVariables.measurementItem.query(ndb.AND(MonitorVariables.measurementItem.m_name==item_name_,
				MonitorVariables.switchItem.d_name==dam_name_)).get()
			if itemCheck_ is None:
				pin_check = MonitorVariables.measurementItem.query(ndb.AND(MonitorVariables.measurementItem.d_name==dam_name_,
				MonitorVariables.measurementItem.arduino_pin== arduino_pin_)).get()
				if (pin_check is None):
					MonitorVariables.measurementItem(d_name=dam_name_,m_name=item_name_, arduino_pin=arduino_pin_).put()
					#MonitorVariables.measurementlog(d_name=dam_name_,m_name=item_name_).put()
					status_ = 'Measurement item added'
				else :
					status_ = 'Pin already assigned'
			else:
				status_ = 'Measurement item already exists'
		return StatusOutput(status = status_)

	@endpoints.method(ITEM_RESOURCE, StatusOutput,
		path="measureDelete", http_method="POST",
		name="measureDelete")
	def measureDelete(self,request):
		"""Delete a specific measurement Item at a dam"""
		item_name_ = request.item_name
		dam_name_ = request.dam_name
		itemCheck_ = MonitorVariables.measurementItem.query(ndb.AND(MonitorVariables.measurementItem.d_name==dam_name_,
				MonitorVariables.measurementItem.m_name==item_name_)).get()
		if itemCheck_ is None:
			status_ = 'Item does not exist'
		else:
			measureItem_ = MonitorVariables.measurementItem.query(ndb.AND(MonitorVariables.measurementItem.d_name==dam_name_,
				MonitorVariables.measurementItem.m_name==item_name_)).fetch()
			for i in measureItem_:
				i.key.delete()
			status_ = 'Item deleted'
		return StatusOutput(status = status_)

	@endpoints.method(DAM_RESOURCE, StatusOutput,
		path="measureList", http_method="POST",
		name="measureList")
	def measureList(self,request):
		"""List all the current items at a dam"""
		dam_name_ = request.dam_name
		dams_list = MonitorVariables.measurementItem.query(MonitorVariables.measurementItem.d_name==dam_name_).fetch()
		#logging.info(dams_list)
		items = ''
		for item in dams_list:
			items += (' Item name:%s, Value:%s, Pin number:%s/'% (item.m_name, item.value, item.arduino_pin))
			items += ''
		#logging.info(damsss)
		return StatusOutput(status= items)

	@endpoints.method(MEASUREMENT_RESOURCE, StatusOutput,
		path="measureChange", http_method="POST",
		name="measureChange")
	def measureChange(self,request):
		"""Change the value of a Measurement item at at dam"""
		dam_name_ = request.dam_name
		item_name_ = request.item_name
		value_ = round(request.value/35.0,2)
		itemCheck_ = MonitorVariables.measurementItem.query(ndb.AND(MonitorVariables.measurementItem.d_name==dam_name_,
				MonitorVariables.measurementItem.m_name==item_name_)).get()
		if (itemCheck_ is None):
			status_ = 'item does not exist'
		else:
			Item_ = MonitorVariables.measurementItem.query(ndb.AND(MonitorVariables.measurementItem.d_name==dam_name_,
				MonitorVariables.measurementItem.m_name==item_name_)).fetch()

			for i in Item_:
				logging.info(i.value_array)
				i.value = value_
				#z = MonitorVariables.measurementlog()
				#for x in i.value_array:
					#z = x , MonitorVariables.measurementlog(array_values=value_)
				i.value_array.append(MonitorVariables.measurementlog(array_values=value_))
				i.put()

			status_ = ('Measurement change to %s' %(value_))
			#array=MonitorVariables.measurementlog.query(ndb.AND(MonitorVariables.measurementlog.d_name==dam_name_,
			#	MonitorVariables.measurementlog.m_name==item_name_)).get()
			#array.array_values = [value_]
			#array.put()
		return StatusOutput(status = status_)

	@endpoints.method(MEASUREMENT_RESOURCE, StatusOutput,
		path="measureView", http_method="POST",
		name="measureView")
	def measureViewHistory(self,request):
		dam_name_ = request.dam_name
		item_name_ = request.item_name
		value_ = request.value
		array = MonitorVariables.measurementItem.query(ndb.AND(MonitorVariables.measurementItem.d_name==dam_name_,
				MonitorVariables.measurementItem.m_name==item_name_)).fetch()
		items=''
		logging.info(array[0].value_array)
		for data in array[0].value_array:
		#	logging.info(data)
			items += (' Value: %s ,Date: %s|'% (data.array_values,data.date_time))
			items += ''
		return StatusOutput(status= items)

	@endpoints.method(PING_RESOURCE, switchStatus,
		path="checkSwitch", http_method="POST",
		name="checkSwitch")
	def get_switch_status(self,request):
		"""Check status of switches"""
		logging.error('About to get query')
		dbitem = MonitorVariables.inputSwitches.query().get()
		if (dbitem is None):
			MonitorVariables.inputSwitches(switch1=False,switch2=False,switch3=False).put()
			
			logging.error('Just created a new variable')
		dbitem = MonitorVariables.inputSwitches.query().fetch()
		
		return switchStatus(switch1=dbitem[0].switch1,switch2=dbitem[0].switch2,switch3=dbitem[0].switch3)
	
    
APPLICATION = endpoints.api_server([monitorDamAPI])
