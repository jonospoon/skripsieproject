import os
import webapp2
import jinja2
import logging
from google.appengine.ext import ndb
import MonitorVariables as mon

JINJA_ENVIRONMENT = jinja2.Environment(loader=jinja2.FileSystemLoader(os.path.dirname(__file__)), 
extensions=['jinja2.ext.autoescape'], autoescape=True)


class listDams(webapp2.RequestHandler):
	def get(self):
		dams_list = mon.damItem.query().fetch()
		logging.info(dams_list)
		dam_temp =''
		damsss = []
		for dam in dams_list:
			if (dam_temp != dam.d_name):
				dam_temp = dam.d_name
				damsss.append(mon.damItem(d_name=dam_temp))
		logging.info(damsss)
		template_values = {
			'dams' : damsss
	   	}
		
		template = JINJA_ENVIRONMENT.get_template('dams.html')
		self.response.write(template.render(template_values))


class createDam(webapp2.RequestHandler):
	def post(self):
		damName_new = self.request.get('dam_name')
		new_dam = mon.damItem.query(mon.damItem.d_name==damName_new).get()
		logging.info(new_dam)
		if (new_dam is None):
			mon.damItem(d_name=damName_new).put()
			mon.switchChangeCheck(d_name=damName_new, change=False ).put()
			outStatus = "New dam entered into the system"
		else:
			outStatus = "Dam already exists"

		template_values = {
			'response': outStatus,
			'switch' : ''
	   	}
		self.response.write(outStatus)

class deleteDam(webapp2.RequestHandler):
	def post(self):
		dam_name_ =self.request.get('dam_name')
		dam_ = mon.damItem.query(mon.damItem.d_name == dam_name_).get()
		if (dam_ is None):
			status_ = 'Dam does not exist'
		else:
			dam_ = mon.damItem.query(mon.damItem.d_name == dam_name_).fetch()
			switchChange = mon.switchChangeCheck.query(mon.switchChangeCheck.d_name==dam_name_).fetch()
			for switchChange_ in switchChange:
				switchChange_.key.delete()
			for dams in dam_:
				dams.key.delete()
			status_ = 'Dam deleted'
		template_values = {
			'response': '',
			'switch' : ''
	   	}
		self.response.write(status_)

class checkDam(webapp2.RequestHandler):
	def post(self):
		dam_name_ = self.request.get('dam_name')
		dam_ = mon.damItem.query(mon.damItem.d_name == dam_name_).get()
		switch_check = ''
		if (dam_ is None):
			"""Dam does not exist"""
			status_ = 'False'
		else:
			"""Dam exists"""
			status_ ='True'
			#switch_check =mon.switchItem.query(mon.switchItem.d_name==dam_name_).fetch()
		#logging.info(switch_check)
		self.response.write(status_)
		