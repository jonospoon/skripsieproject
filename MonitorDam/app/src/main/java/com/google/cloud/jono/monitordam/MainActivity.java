package com.google.cloud.jono.monitordam;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.appspot.skripsie_jono.monitorDamAPI.MonitorDamAPI;
import com.appspot.skripsie_jono.monitorDamAPI.model.MonitorDamAPIDams;
import com.appspot.skripsie_jono.monitorDamAPI.model.MonitorDamAPIItemGeneral;
import com.appspot.skripsie_jono.monitorDamAPI.model.MonitorDamAPIMeasurements;
import com.appspot.skripsie_jono.monitorDamAPI.model.MonitorDamAPIStatusOutput;
import com.appspot.skripsie_jono.monitorDamAPI.model.MonitorDamAPISwitchState;

import org.shokai.firmata.ArduinoFirmata;
import org.shokai.firmata.ArduinoFirmataEventHandler;

import java.io.IOException;
import java.util.HashMap;

import static java.lang.StrictMath.abs;

public class MainActivity extends AppCompatActivity {
    private TextView tv;
    private String TAG = "Sample";
    private Handler handler;
    private ArduinoFirmata arduino;
    private boolean LED1_b = false;
    private int digital1 = 10;
    private int digital2 = 11;
    private int digital3 = 12;
    private int LED1 = 5;
    private int LED2 = 6;
    private int LED3 = 7;
    private int RELAY1 = 2;
    private int RELAY2 = 4;
    private String dam = "test";
    private TextView textAnalogRead;
    private Thread thread;
    private boolean initialized = false;
    private int finished_loading = 3;
    PendingIntent pendingIntent;
    //used to store running alarmmanager instance
    AlarmManager alarmManager;
    //Callback function for Alarmmanager event
    BroadcastReceiver mReceiver;
   // public String [] switches = new String[13];
    UsbManager manager;
    private boolean inifity;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (initialized == false) {
            Log.v("TAG", "About to Load switch Data");
            LoadSwitchData();
            Log.v("TAG", "About to Load Measure Data");
            LoadMeasureData();
            for (int i =0; i < AppConstants.switches.length; i++)
            {
                AppConstants.switches[i]="None";
            }
        }
        // Prevent the keyboard from being visible upon startup.
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        this.textAnalogRead = (TextView)findViewById(R.id.text_analog_read);
        tv = (TextView) findViewById(R.id.textView);
        inifity = false;
        this.arduino = new ArduinoFirmata(this);
        final Activity self = this;
        arduino.setEventHandler(new ArduinoFirmataEventHandler() {
            @Override
            public void onClose() {
                Log.v(TAG, "arduino closed");
                self.finish();
            }

            @Override
            public void onError(String errorMessage) {
                Log.e(TAG, errorMessage);
            }
        });
        RegisterAlarmBroadcast();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    protected void onDestroy() {
        unregisterReceiver(mReceiver);
        super.onDestroy();
    }
    public void onClickUnregisterAlarmBroadcast(View view) {
        alarmManager.cancel(pendingIntent);
        //getBaseContext().unregisterReceiver(mReceiver);
    }

    public void onClickSetAlarm(View v)
    {
        //Get the current time and set alarm after 10 seconds from current time
        // so here we get
        setAlarm();
    }
    public void setAlarm(){
        alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis()+1000*15, pendingIntent);
    }
    private void RegisterAlarmBroadcast()
    {
        Log.i(TAG,"Going to register Intent.RegisterAlramBroadcast");

        //This is the call back function(BroadcastReceiver) which will be call when your
        //alarm time will reached.
        mReceiver = new BroadcastReceiver()
        {
            private static final String TAG = "Alarm Example Receiver";
            @Override
            public void onReceive(Context context, Intent intent)
            {
                Log.i(TAG, "BroadcastReceiver::OnReceive() >>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                tv.append("About to start: "+finished_loading);
                if (finished_loading>2){
                    tv.append("Inside: "+finished_loading);

                    finished_loading = 0;
                    Toast.makeText(context, "Post to server"+finished_loading, Toast.LENGTH_SHORT).show();
                    checkAll();
                }
                finished_loading+=2;



                setAlarm();
            }
        };

        // register the alarm broadcast here
        registerReceiver(mReceiver, new IntentFilter("com.techblogon.alarmexample") );
        pendingIntent = PendingIntent.getBroadcast( this, 0, new Intent("com.techblogon.alarmexample"),0 );
        alarmManager = (AlarmManager)(this.getSystemService( Context.ALARM_SERVICE ));
    }

    public void addSwitch(View view){
        Intent intent = new Intent(this, addSwitchActivity.class);
        startActivity(intent);

    }
    public void LoadMeasureData (){
        AsyncTask<Void, Void, MonitorDamAPIStatusOutput> getAndDisplaySwitchStatus = new
                AsyncTask<Void, Void, MonitorDamAPIStatusOutput>() {
                    @Override
                    protected MonitorDamAPIStatusOutput doInBackground(Void... params) {
                        //Retrieve service handle
                        MonitorDamAPI apiServiceHandle = AppConstants.getApiServiceHandle();
                        MonitorDamAPIDams dam_ = new MonitorDamAPIDams();
                        dam_.setDamName(dam);
                        try{
                            MonitorDamAPI.MeasureList getSwitches = apiServiceHandle.measureList(dam_);
                            Log.v("TAG", "Sending Measure data away");
                            MonitorDamAPIStatusOutput output = getSwitches.execute();
                            return output;
                        }catch (IOException e){
                            Log.e(TAG, "No switches were returned by the API.");
                        }
                        return null;
                    }
                    @Override
                    protected void onPostExecute(MonitorDamAPIStatusOutput item_state){
                        Log.v("TAG", "Measure Data returned");
                        if(!item_state.getStatus().equals("")){
                            textAnalogRead.setText(item_state.getStatus().toString());
                            String [] a_item = item_state.getStatus().split("/");
                            for (int i = 0; i < a_item.length; i++){
                                String [] each_part = a_item[i].split(",");
                                int pinNumber = Integer.parseInt(each_part[2].split(":")[1]);
                                double lastMeasure = Double.parseDouble(each_part[1].split(":")[1]);
                                String name = each_part[0].split(":")[1];
                                AppConstants.measurementItems[pinNumber]= name;
                                AppConstants.lastMeasurementItem[pinNumber]= lastMeasure;
                                AppConstants.measurementUsed[pinNumber] = true;

                            }
                        }
                        else
                        {
                            Log.e(TAG, "Nothing was returned by the API");
                        }
                    }
                };
        getAndDisplaySwitchStatus.execute();
    }
    public void LoadSwitchData(){
        AsyncTask<Void, Void, MonitorDamAPIStatusOutput> getAndDisplaySwitchStatus = new
                AsyncTask<Void, Void, MonitorDamAPIStatusOutput>() {
                    @Override
                    protected MonitorDamAPIStatusOutput doInBackground(Void... params) {
                        //Retrieve service handle
                        MonitorDamAPI apiServiceHandle = AppConstants.getApiServiceHandle();
                        MonitorDamAPIDams dam_ = new MonitorDamAPIDams();
                        dam_.setDamName(dam);

                        try{
                            MonitorDamAPI.SwitchList getSwitches = apiServiceHandle.switchList(dam_);
                            Log.v("TAG", "Sending switch data away");
                            MonitorDamAPIStatusOutput switches = getSwitches.execute();
                            return switches;
                        }catch (IOException e){
                            Log.e(TAG, "No switches were returned by the API.");
                        }
                        return null;
                    }
                    @Override
                    protected void onPostExecute(MonitorDamAPIStatusOutput switch_State){
                        Log.v("TAG", "Switch Data returned");
                        if(!switch_State.getStatus().equals("")){
                            //tv.setText(switch_State.getStatus().toString());
                            String [] a_switch = switch_State.getStatus().split("/");
                            //tv.append(a_switch.length+"");

                            for (int i = 0; i < a_switch.length; i++){
                                String [] each_part = a_switch[i].split(",");
                                int pinNumber = Integer.parseInt(each_part[1].split(":")[1]);
                                String name = each_part[0].split(":")[1];
                                boolean Input = Boolean.parseBoolean(each_part[2].split(":")[1]);
                                boolean state =  Boolean.parseBoolean(each_part[3].split(":")[1]);
                                AppConstants.switches[pinNumber] = name;
                                AppConstants.switch_Input[pinNumber] = Input;
                                AppConstants.switch_state[pinNumber] = state;
                                tv.append(pinNumber+" "+name+" "+ Input+" "+state);
                            }
                        }
                        else
                        {
                            Log.e(TAG,"Nothing was returned by the API");
                        }
                    }
                };
        getAndDisplaySwitchStatus.execute();
    }


    public void onClickListDams(View view){
        View rootView = view.getRootView();
        AsyncTask<Void, Void, MonitorDamAPIStatusOutput> getAndDisplaySwitchStatus = new
                AsyncTask<Void, Void, MonitorDamAPIStatusOutput>() {
            @Override
            protected MonitorDamAPIStatusOutput doInBackground(Void... params) {
                //Retrieve service handle
                MonitorDamAPI apiServiceHandle = AppConstants.getApiServiceHandle();
                try{
                    MonitorDamAPI.DamList getSwitches = apiServiceHandle.damList();
                    MonitorDamAPIStatusOutput switches = getSwitches.execute();
                    return switches;
                }catch (IOException e){
                    Log.e(TAG, "No switches were returned by the API.");
                }
                return null;
            }
                    @Override
                    protected void onPostExecute(MonitorDamAPIStatusOutput switch_State){
                    if(switch_State != null){
                        tv.setText(switch_State.getStatus().toString());
                    }
                        else
                    {
                        Log.e(TAG,"Nothing was returned by the API");
                    }
                    }
        };
        getAndDisplaySwitchStatus.execute();
    }
    public void onClickchangeSwitch(View view){
        AsyncTask<Void, Void, MonitorDamAPIStatusOutput> getAndDisplaySwitchStatus = new
                AsyncTask<Void, Void, MonitorDamAPIStatusOutput>() {
                    @Override
                    protected MonitorDamAPIStatusOutput doInBackground(Void... params) {
                        //Retrieve service handle
                        MonitorDamAPI apiServiceHandle = AppConstants.getApiServiceHandle();

                        String switch_name = "s1";
                        boolean state = !arduino.digitalRead(digital1);
                        MonitorDamAPISwitchState content = new MonitorDamAPISwitchState();
                        content.setDamName(dam);
                        content.setStateOnNoff(state);
                        content.setSwitchName(switch_name);
                        try{
                            MonitorDamAPI.SwitchChange switcher = apiServiceHandle.switchChange(content);
                            MonitorDamAPIStatusOutput switches = switcher.execute();
                            return switches;
                        }catch (IOException e){
                            Log.e(TAG, "No switches were returned by the API.");
                        }
                        return null;
                    }
                    @Override
                    protected void onPostExecute(MonitorDamAPIStatusOutput switch_State){
                        if(switch_State != null){
                            tv.setText(switch_State.getStatus().toString());
                        }
                        else
                        {
                            Log.e(TAG,"Nothing was returned by the API");
                        }
                    }
                };
        getAndDisplaySwitchStatus.execute();
    }

    public void onClickconnectDevice(View view) {
            manager = (UsbManager) getSystemService(Context.USB_SERVICE);
            HashMap<String, UsbDevice> deviceList = manager.getDeviceList();



                try{
                    arduino.connect();
                    for (int i = 0; i < AppConstants.switch_Input.length; i ++)
                    {
                        tv.append(AppConstants.switch_Input[i] + "");
                        if (AppConstants.switch_Input[i]==true)
                        {
                            arduino.pinMode(i,ArduinoFirmata.INPUT);
                        }
                        else if (AppConstants.switch_Input[i]== false){
                            if (AppConstants.switch_state[i]== true) {
                                arduino.digitalWrite(i, AppConstants.switch_state[i]);
                            }
                        }
                    }
                    Toast.makeText(this,"Connected to "+arduino.getBoardVersion(), Toast.LENGTH_SHORT).show();

                }
                catch(IOException e){
                    e.printStackTrace();
                    finish();
                }
                catch (InterruptedException e)
                {
                    finish();
                }


        }
    public void onClickUpdateRelay1(){
        AsyncTask<Void, Void, MonitorDamAPISwitchState> getAndDisplaySwitchStatus = new
                AsyncTask<Void, Void, MonitorDamAPISwitchState>() {
                    @Override
                    protected MonitorDamAPISwitchState doInBackground(Void... params) {
                        //Retrieve service handle
                        MonitorDamAPI apiServiceHandle = AppConstants.getApiServiceHandle();
                        MonitorDamAPIItemGeneral content_relay1 = new MonitorDamAPIItemGeneral();
                        content_relay1.setDamName(dam);
                        content_relay1.setItemName("Relay1");

                        try{
                            MonitorDamAPI.SwitchStatus Relay_1 = apiServiceHandle.switchStatus(content_relay1);
                            MonitorDamAPISwitchState switches2 = Relay_1.execute();
                            return switches2;
                        }catch (IOException e){
                            Log.e(TAG, "No switches were returned by the API.");
                        }
                        return null;
                    }
                    @Override
                    protected void onPostExecute(MonitorDamAPISwitchState switch_State){
                        if(switch_State != null){
                            arduino.digitalWrite(RELAY1,switch_State.getStateOnNoff());
                            tv.append("Relay1:"+ switch_State.getStateOnNoff());
                        }
                        else
                        {
                            Log.e(TAG,"Nothing was returned by the API");
                        }
                    }
                };
        getAndDisplaySwitchStatus.execute();
    }
    public void onClickUpdateRelay2(){
        AsyncTask<Void, Void, MonitorDamAPISwitchState> getAndDisplaySwitchStatus = new
                AsyncTask<Void, Void, MonitorDamAPISwitchState>() {
                    @Override
                    protected MonitorDamAPISwitchState doInBackground(Void... params) {
                        //Retrieve service handle
                        MonitorDamAPI apiServiceHandle = AppConstants.getApiServiceHandle();
                        MonitorDamAPIItemGeneral content_relay2 = new MonitorDamAPIItemGeneral();
                        content_relay2.setDamName(dam);
                        content_relay2.setItemName("Relay2");

                        try{
                            MonitorDamAPI.SwitchStatus Relay_2 = apiServiceHandle.switchStatus(content_relay2);
                            MonitorDamAPISwitchState switches2 = Relay_2.execute();
                            return switches2;
                        }catch (IOException e){
                            Log.e(TAG, "No switches were returned by the API.");
                        }
                        return null;
                    }
                    @Override
                    protected void onPostExecute(MonitorDamAPISwitchState switch_State){
                        if(switch_State != null){
                            arduino.digitalWrite(RELAY2,switch_State.getStateOnNoff());
                            tv.append("Relay2:"+ switch_State.getStateOnNoff());
                        }
                        else
                        {
                            Log.e(TAG,"Nothing was returned by the API");
                        }
                    }
                };
        getAndDisplaySwitchStatus.execute();
    }



    public void onClickUpdateLed2(){
        AsyncTask<Void, Void, MonitorDamAPISwitchState> getAndDisplaySwitchStatus = new
                AsyncTask<Void, Void, MonitorDamAPISwitchState>() {
                    @Override
                    protected MonitorDamAPISwitchState doInBackground(Void... params) {
                        //Retrieve service handle
                        MonitorDamAPI apiServiceHandle = AppConstants.getApiServiceHandle();
                        MonitorDamAPIItemGeneral content_led2 = new MonitorDamAPIItemGeneral();
                        content_led2.setDamName(dam);
                        content_led2.setItemName("Led2");

                        try{
                            MonitorDamAPI.SwitchStatus LED_2 = apiServiceHandle.switchStatus(content_led2);
                            MonitorDamAPISwitchState switches2 = LED_2.execute();
                            return switches2;
                        }catch (IOException e){
                            Log.e(TAG, "No switches were returned by the API.");
                        }
                        return null;
                    }
                    @Override
                    protected void onPostExecute(MonitorDamAPISwitchState switch_State){
                        if(switch_State != null){
                            LED1_b = switch_State.getStateOnNoff();
                            arduino.digitalWrite(LED2,switch_State.getStateOnNoff());
                            tv.append("Led2:"+ switch_State.getStateOnNoff());
                        }
                        else
                        {
                            Log.e(TAG,"Nothing was returned by the API");
                        }
                    }
                };
        getAndDisplaySwitchStatus.execute();
    }
    public void onClickUpdateLed3(){
        AsyncTask<Void, Void, MonitorDamAPISwitchState> getAndDisplaySwitchStatus = new
                AsyncTask<Void, Void, MonitorDamAPISwitchState>() {
                    @Override
                    protected MonitorDamAPISwitchState doInBackground(Void... params) {
                        //Retrieve service handle
                        MonitorDamAPI apiServiceHandle = AppConstants.getApiServiceHandle();
                        MonitorDamAPIItemGeneral content_led3 = new MonitorDamAPIItemGeneral();
                        content_led3.setDamName(dam);
                        content_led3.setItemName("Led3");

                        try{
                            MonitorDamAPI.SwitchStatus LED_3 = apiServiceHandle.switchStatus(content_led3);
                            MonitorDamAPISwitchState switches2 = LED_3.execute();
                            return switches2;
                        }catch (IOException e){
                            Log.e(TAG, "No switches were returned by the API.");
                        }
                        return null;
                    }
                    @Override
                    protected void onPostExecute(MonitorDamAPISwitchState switch_State){
                        if(switch_State != null){
                            LED1_b = switch_State.getStateOnNoff();
                            arduino.digitalWrite(LED3,switch_State.getStateOnNoff());
                            tv.append("Led3:"+ switch_State.getStateOnNoff());
                        }
                        else
                        {
                            Log.e(TAG,"Nothing was returned by the API");
                        }
                    }
                };
        getAndDisplaySwitchStatus.execute();
    }

    public void onClickUpdateLed1(View view) {
        AsyncTask<Void, Void, MonitorDamAPISwitchState> getAndDisplaySwitchStatus = new
                AsyncTask<Void, Void, MonitorDamAPISwitchState>() {

                    @Override
                    protected MonitorDamAPISwitchState doInBackground(Void... params) {

                        //Retrieve service handle
                        MonitorDamAPI apiServiceHandle = AppConstants.getApiServiceHandle();
                        MonitorDamAPIItemGeneral content_led1 = new MonitorDamAPIItemGeneral();
                        content_led1.setDamName(dam);
                        content_led1.setItemName("Led1");

                        try{
                            MonitorDamAPI.SwitchStatus LED_1 = apiServiceHandle.switchStatus(content_led1);
                            MonitorDamAPISwitchState switches = LED_1.execute();
                            return switches;
                        }catch (IOException e){
                            Log.e(TAG, "No switches were returned by the API.");
                        }

                        return null;

                    }
                    @Override
                    protected void onPostExecute(MonitorDamAPISwitchState switch_State){
                        if(switch_State != null){
                            LED1_b = switch_State.getStateOnNoff();
                            arduino.digitalWrite(LED1,switch_State.getStateOnNoff());
                            tv.append("Led1:" + switch_State.getStateOnNoff());
                        }
                        else
                        {
                            Log.e(TAG,"Nothing was returned by the API");
                        }
                    }
                };
        getAndDisplaySwitchStatus.execute();
    }


    public void onClickInfinity(final View view) {

        AsyncTask<Void, Void, Void> getAndDisplaySwitchStatus = new
                AsyncTask<Void, Void, Void>() {
                    @Override
                    protected Void doInBackground(Void... params) {
                        //Retrieve service handle

                        while(inifity) {

                            onClickUpdateLed1(view);
                        }

                        return null;
                    }
                    @Override
                    protected void onPostExecute(Void switch_State){
                        tv.setText(switch_State.toString());
                        if(switch_State != null){

                        }
                        else
                        {
                            Log.e(TAG, "Nothing was returned by the API");
                        }
                    }
                };
        getAndDisplaySwitchStatus.execute();
        while(inifity)
        {
            Toast.makeText(this,"Now in an infinite loop", Toast.LENGTH_SHORT).show();
            onClickchangeSwitch(view);
            onClickUpdateLed1(view);
        }
    }
    private void checkAll(){
        Log.v("TAG", "About to Check all");
        CheckSwitches();
        CheckMeasurementStatus();
        CheckArduinoStatus();
        Log.v("TAG", "CheckAll complete");
    }

    public void onClickcheckAll(final View view){
        checkAll();
    }

    public void CheckMeasurementStatus(){
        tv.setText("");
        for (int i = 0; i < AppConstants.measurementItems.length; i++){


            if (AppConstants.measurementUsed[i]){
                double current_measure = arduino.analogRead(i)/35.0;
                double current_dif = abs(AppConstants.lastMeasurementItem[i] - current_measure);
                tv.append(current_dif+"");
                if (current_dif > 0.1 ){
                    tv.append("Big enough diff");
                    changeAnalogData(i);
                    AppConstants.lastMeasurementItem[i] = current_measure;
                }

            }

        }
    }

    public void CheckArduinoStatus(){

        for(int i = 0; i <AppConstants.switches.length; i++)
        {
            if (AppConstants.switch_Input[i]== true)
            {   Log.v("TAG", "Checking Arduino");
                boolean current_state = !arduino.digitalRead(i);
                Log.v("TAG", "Arduino check complete");
                tv.setText(AppConstants.switches[i]+" ");
                tv.append(AppConstants.switch_state[i]+ " "+ current_state);
                if (AppConstants.switch_state[i] != current_state){
                    changeSwitchData(i);
                    AppConstants.switch_state[i] = current_state;
                }
            }
        }

    }

    public void changeSwitchData(final int pin_number){

        AsyncTask<Void, Void, MonitorDamAPIStatusOutput> getAndDisplaySwitchStatus = new
                AsyncTask<Void, Void, MonitorDamAPIStatusOutput>() {
                    @Override
                    protected MonitorDamAPIStatusOutput doInBackground(Void... params) {
                        //Retrieve service handle
                        MonitorDamAPI apiServiceHandle = AppConstants.getApiServiceHandle();

                        String switch_name = AppConstants.switches[pin_number];

                        boolean state = !arduino.digitalRead(pin_number);
                        MonitorDamAPISwitchState content = new MonitorDamAPISwitchState();
                        content.setDamName(dam);
                        content.setStateOnNoff(state);
                        content.setSwitchName(switch_name);
                        try{
                            MonitorDamAPI.SwitchChange switcher = apiServiceHandle.switchChange(content);
                            MonitorDamAPIStatusOutput switches = switcher.execute();
                            return switches;
                        }catch (IOException e){
                            Log.e(TAG, "No switches were returned by the API.");
                        }
                        return null;
                    }
                    @Override
                    protected void onPostExecute(MonitorDamAPIStatusOutput switch_State){
                        if(switch_State != null){
                            tv.setText(switch_State.getStatus().toString());

                        }
                        else
                        {
                            Log.e(TAG, "Nothing was returned by the API");
                        }
                        finished_loading+=1;
                    }
                };
        getAndDisplaySwitchStatus.execute();
    }


    public void CheckSwitches() {
        AsyncTask<Void, Void, MonitorDamAPIStatusOutput> getAndDisplaySwitchStatus = new
                AsyncTask<Void, Void, MonitorDamAPIStatusOutput>() {
                    @Override
                    protected MonitorDamAPIStatusOutput doInBackground(Void... params) {
                        //Retrieve service handle
                        MonitorDamAPI apiServiceHandle = AppConstants.getApiServiceHandle();
                        MonitorDamAPIDams dams = new MonitorDamAPIDams();
                        dams.setDamName(dam);
                        try{
                            MonitorDamAPI.CheckForChange getStatus = apiServiceHandle.checkForChange(dams);
                            MonitorDamAPIStatusOutput switches = getStatus.execute();
                            return switches;
                        }catch (IOException e){
                            Log.e(TAG, "No switches were returned by the API.");
                        }
                        return null;

                    }
                    @Override
                    protected void onPostExecute(MonitorDamAPIStatusOutput switches){

                        String possible_switches = switches.getStatus();
                        //tv.setText(possible_switches);
                        if(possible_switches.contentEquals("None")){
                           // tv.setText("No switches were changed");
                            Log.e(TAG, "No switches were changed");
                        }
                        else
                        {
                            String [] individual_switches = possible_switches.split(",");
                            tv.append("number of changes = " + individual_switches.length);
                            for(int i = 0; i <individual_switches.length; i++) {
                                int pin_number = Integer.parseInt(individual_switches[i]);
                                tv.append(", Change is: " + AppConstants.switches[pin_number]);
                                AppConstants.switch_state[pin_number]= !AppConstants.switch_state[pin_number];
                                if (arduino.isOpen()) {
                                    arduino.digitalWrite(pin_number, AppConstants.switch_state[pin_number]);
                                    tv.append("Changed");
                                }
                                else {
                                    tv.setText("Please attach arduino");
                                }

                            }

                        }
                        finished_loading+=1;
                    }
                };
        getAndDisplaySwitchStatus.execute();

    }
    public void changeAnalogData(final int pin_number){
        AsyncTask<Void, Void, MonitorDamAPIStatusOutput> getAndDisplaySwitchStatus = new
                AsyncTask<Void, Void, MonitorDamAPIStatusOutput>() {
                    @Override
                    protected MonitorDamAPIStatusOutput doInBackground(Void... params) {
                        //Retrieve service handle
                        MonitorDamAPI apiServiceHandle = AppConstants.getApiServiceHandle();
                        MonitorDamAPIMeasurements content_m1 = new MonitorDamAPIMeasurements();
                        content_m1.setDamName(dam);
                        content_m1.setItemName(AppConstants.measurementItems[pin_number]);
                        content_m1.setValue((long)arduino.analogRead(pin_number));

                        try{
                            MonitorDamAPI.MeasureChange M_1 = apiServiceHandle.measureChange(content_m1);
                            MonitorDamAPIStatusOutput measure1 = M_1.execute();
                            return measure1;
                        }catch (IOException e){
                            Log.e(TAG, "No switches were returned by the API.");
                        }
                        return null;
                    }
                    @Override
                    protected void onPostExecute(MonitorDamAPIStatusOutput output){

                        if(output != null){
                            textAnalogRead.append(output.getStatus());
                        }
                        else
                        {
                            Log.e(TAG,"Nothing was returned by the API");
                        }
                        finished_loading+=1;
                    }
                };
        getAndDisplaySwitchStatus.execute();

    }

    public void onClickAnalogRead(final View view) {
        CheckMeasurementStatus();
  /*
        AsyncTask<Void, Void, MonitorDamAPIStatusOutput> getAndDisplaySwitchStatus = new
                AsyncTask<Void, Void, MonitorDamAPIStatusOutput>() {
                    @Override
                    protected MonitorDamAPIStatusOutput doInBackground(Void... params) {
                        //Retrieve service handle
                        MonitorDamAPI apiServiceHandle = AppConstants.getApiServiceHandle();
                        MonitorDamAPIMeasurements content_m1 = new MonitorDamAPIMeasurements();
                        content_m1.setDamName(dam);
                        content_m1.setItemName("m1");
                        content_m1.setValue((long)arduino.analogRead(1));

                        try{
                            MonitorDamAPI.MeasureChange M_1 = apiServiceHandle.measureChange(content_m1);
                            MonitorDamAPIStatusOutput measure1 = M_1.execute();
                            return measure1;
                        }catch (IOException e){
                            Log.e(TAG, "No switches were returned by the API.");
                        }
                        return null;
                    }
                    @Override
                    protected void onPostExecute(MonitorDamAPIStatusOutput output){
                        if(output != null){
                            textAnalogRead.append(output.getStatus());
                        }
                        else
                        {
                            Log.e(TAG,"Nothing was returned by the API");
                        }
                    }
                };
        getAndDisplaySwitchStatus.execute();*/

    }


}
