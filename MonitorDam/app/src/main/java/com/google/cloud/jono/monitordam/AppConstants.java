package com.google.cloud.jono.monitordam;


import com.appspot.skripsie_jono.monitorDamAPI.MonitorDamAPI;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.extensions.android.json.AndroidJsonFactory;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;

/**
 * Created by BWA on 2015/09/11.
 */
public class AppConstants {
    /**
     * Class instance of the JSON factory.
     */
    public static final JsonFactory JSON_FACTORY = new AndroidJsonFactory();


    /**
     * Class instance of the HTTP transport.
     */
    public static final HttpTransport HTTP_TRANSPORT = AndroidHttp.newCompatibleTransport();


    /**
     * Retrieves a Helloworld api service handle to access the API.
     */
    public static MonitorDamAPI getApiServiceHandle() {
        // Use a builder to help formulate the API request.
        MonitorDamAPI.Builder MonitorDam = new MonitorDamAPI.Builder(AppConstants.HTTP_TRANSPORT,
                AppConstants.JSON_FACTORY,null);


        return MonitorDam.build();
    }

    public static String [] switches = new String[13];
    public static boolean [] switch_Input = new boolean[13];
    public static boolean [] switch_state = new boolean[13];
    public static String [] measurementItems = new String [6];
    public static double [] lastMeasurementItem = new double[6];
    public static boolean [] measurementUsed = new boolean[6];

}
