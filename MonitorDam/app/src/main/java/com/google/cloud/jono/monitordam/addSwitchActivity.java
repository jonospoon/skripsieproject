package com.google.cloud.jono.monitordam;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.appspot.skripsie_jono.monitorDamAPI.MonitorDamAPI;
import com.appspot.skripsie_jono.monitorDamAPI.model.MonitorDamAPIStatusOutput;
import com.appspot.skripsie_jono.monitorDamAPI.model.MonitorDamAPISwitches;

import java.io.IOException;

public class addSwitchActivity extends AppCompatActivity {
    private EditText switch_name_ET;
    private ToggleButton switch_type_tb;
    private EditText arduino_pin_ET;
    private String TAG = "addSwitch";
    private TextView outputText_tv;
    private boolean added;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_switch);
        switch_name_ET = (EditText) findViewById(R.id.etswitchName);
        switch_type_tb = (ToggleButton) findViewById(R.id.tBswitchType);
        arduino_pin_ET = (EditText) findViewById(R.id.etPinName);
        outputText_tv = (TextView) findViewById(R.id.tv_output);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onclickCreateButton(View view) {

        String switch_name = switch_name_ET.getText().toString();
        int arduino_pin =Integer.parseInt(arduino_pin_ET.getText().toString());
        Boolean switch_type = switch_type_tb.isChecked();
        outputText_tv.setText(switch_type + "   " + arduino_pin);
        outputText_tv.append(AppConstants.switches[arduino_pin] + "  ");


        outputText_tv.append("added");
            //main.switches[arduino_pin] = switch_name;
        addSwitch(switch_name, arduino_pin, switch_type);
                //AppConstants.switches[arduino_pin] = switch_name;


    }
    private void addSwitch(final String switch_name, final long arduino_pin, final boolean input_Noutput ){
        AsyncTask<Void, Void, MonitorDamAPIStatusOutput> createNewSwitch = new
                AsyncTask<Void, Void, MonitorDamAPIStatusOutput>() {
                    @Override
                    protected MonitorDamAPIStatusOutput doInBackground(Void... params) {
                        //Retrieve service handle
                        MonitorDamAPI apiServiceHandle = AppConstants.getApiServiceHandle();
                        MonitorDamAPISwitches content_New_switch = new MonitorDamAPISwitches();
                        content_New_switch.setDamName("test");
                        content_New_switch.setSwitchName(switch_name);
                        content_New_switch.setArduinoPin(arduino_pin);
                        content_New_switch.setInputNOToutput(input_Noutput);

                        try{
                            MonitorDamAPI.SwitchCreate new_switch = apiServiceHandle.switchCreate(content_New_switch);
                            MonitorDamAPIStatusOutput output = new_switch.execute();
                            return output;
                        }catch (IOException e){
                            Log.e(TAG, "No switches were returned by the API.");
                        }
                        return null;
                    }
                    @Override
                    protected void onPostExecute(MonitorDamAPIStatusOutput output){
                        if(output != null){
                            outputText_tv.setText(output.getStatus());
                            if ( output.getStatus().equals("switch added")){
                                AppConstants.switches[(int)arduino_pin] = switch_name;
                            }

                        }
                        else
                        {
                            Log.e(TAG, "Nothing was returned by the API");
                        }
                    }
                };
        createNewSwitch.execute();

    }
}
