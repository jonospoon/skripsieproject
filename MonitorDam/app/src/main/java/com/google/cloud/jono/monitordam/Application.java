package com.google.cloud.jono.monitordam;

import com.appspot.skripsie_jono.monitorDamAPI.model.MonitorDamAPIDams;
import com.appspot.skripsie_jono.monitorDamAPI.model.MonitorDamAPIStatusOutput;
import com.appspot.skripsie_jono.monitorDamAPI.model.MonitorDamAPISwitchStatus;
import com.google.api.client.util.Lists;

import java.util.ArrayList;

/**
 * Created by BWA on 2015/09/11.
 */
public class Application extends android.app.Application {
    ArrayList<MonitorDamAPISwitchStatus> switches = Lists.newArrayList();

    ArrayList<MonitorDamAPIDams> dams = Lists.newArrayList();
    ArrayList<MonitorDamAPIStatusOutput> status = Lists.newArrayList();
}


