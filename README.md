# README #

Welcome to a rural Water telemetry outstation. This is a application created for my mini-thesis at Stellenbosch University in order to obtain my degree in Electronic and Electric engineering. The repository consists of a website, a database, an android app and endpoints to connect the online database to the android app. The database was created specifically to be implemented on Google App Engine (GAE), so unfortunately cannot be hosted anywhere else at this time. 

### What is this repository for? ###

* An android app which connects to an arduino and can read and write data to the arduino through a USB connection using USB-On the go. 
* A database which connects to the Android app and the website. 
* Version 1
* [My implementation](https://skripsie-jono.appspot.com/)

### How do I get set up? ###

* Build and load the android app
* Upload the website and database to GAE
* Create a station on the website and ensure it has the same name on the phone.
* Create a switch on the website or app.

### Who do I talk to? ###

* I am no longer actively updating this site. If you would like to branch it feel free to. 
* You can contact me if you have any inquiries.